// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ft_memdump.c                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wide-aze <wide-aze@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/23 15:51:45 by wide-aze          #+#    #+#             //
//   Updated: 2016/09/06 22:13:23 by wide-aze         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <stdio.h> // printf
#include <ctype.h> // isprint

void ft_memdump(const char *mem, int nb_bytes)
{
  for (int pos = 0; pos < nb_bytes; ++pos)
  {
    for (int i = 0; i < 4 && pos < nb_bytes; ++i)
    {
      for (int j = 0; j < 4; ++j)
      {
        for (int k = 0; k < 4; ++k)
       	{
       	  if (pos + 4 * j + k < nb_bytes)
       		  printf("%2.2x ", (unsigned char)(mem[pos + 4 * j + k]));
       		else
       			printf("   ");
       	}
       	printf("  ");
       }
      for (int j = 0; j < 4 && pos < nb_bytes; ++j)
      {
        for (int k = 0; k < 4 && pos < nb_bytes; ++k, ++pos)
          printf("%c", ( isprint((int)mem[pos] & 0xFF)) ? mem[pos] : '.');
        printf(" ");
      }
      printf("\n");
    }
    printf("\n");
  }
  fflush(stdout);
}
